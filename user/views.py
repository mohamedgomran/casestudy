from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse
from django.views.generic import View
from .forms import userform
from .models import CustomeUser
from django.core.validators import validate_email
from django.views.generic import ListView

def list(request):
    return HttpResponse("sdfsdf")


class UserFormView(View):
    template_path = 'add.html'
    user_form = userform

    def get(self, request):
        form = self.user_form(None)
        return render(request, self.template_path, {'form': form})

    def post(self, request):
        form = self.user_form(request.POST)

        try:
            validate_email(form.data['email'])
        except Exception as e:
            form.add_error('email', 'Enter a valid email')
            return render(request, self.template_path, {'form': form})

        if CustomeUser.objects.filter(email=form.data['email']):
            return render(request, self.template_path, {'form': form})

        if form.is_valid():
            form.save()
            return redirect('/list/')
        return render(request, self.template_path, {'form': form})


class UserListView(ListView):
    model = CustomeUser
    template_name = 'list.html'
    context_object_name = "user_list"
    paginate_by = 10