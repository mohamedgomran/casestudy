from django.db import models

# Create your models here.
class CustomeUser(models.Model):
	"""docstring for Category"""
	full_name = models.CharField(max_length=(70))
	email = models.CharField(max_length=(254), unique=True)
