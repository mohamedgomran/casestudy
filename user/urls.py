from django.urls import path
from . import views


urlpatterns = [
    path('', views.UserFormView.as_view()),
    path('list/', views.UserListView.as_view()),
    path('add/', views.UserFormView.as_view()),

]
