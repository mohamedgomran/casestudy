from django import forms
from .models import CustomeUser


class userform(forms.ModelForm):

    class Meta:
        model = CustomeUser
        fields = ['full_name', 'email']
